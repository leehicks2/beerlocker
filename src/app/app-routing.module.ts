import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BeerListComponent } from './beerlist/beerlist.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

import { AuthGuard } from './service/authgaurd.service';
//import { AuthgaurdService as AuthGuard } from './service/authgaurd.service';

const routes: Routes = [
	{ path: 'beerlist', component: BeerListComponent, canActivate: [AuthGuard]},
	{ path: 'home', component: HomeComponent},
	{ path: 'login', component: LoginComponent},
	{ path: '', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
