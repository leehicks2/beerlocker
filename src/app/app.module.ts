import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {DataTableModule} from "angular2-datatable";
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BeerListComponent } from './beerlist/beerlist.component';

import { BeerService } from './service/beer.service';
import { LoginService } from './service/login.service';
import { AuthGuard } from './service/authgaurd.service';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

@NgModule({
  declarations: [
    AppComponent,
    BeerListComponent,
    HomeComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
	DataTableModule,
	HttpModule,
	FormsModule
  ],
  providers: [BeerService,LoginService,AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
