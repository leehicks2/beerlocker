import { Injectable } from '@angular/core';
import { Http, Response,RequestOptions,Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

import { Beer } from '../beer';
import { BeerNew } from '../beernew';

import { LoginService } from '../service/login.service';

@Injectable({
  providedIn: 'root'
})
export class BeerService {
	
	constructor(private http:Http,private loginService:LoginService) { }
	
	beerList = [];
	beer = new Beer();
	

	url = "http://test.nologo.studio:8080/api/beer";
	getBeers(): Observable<Beer[]> {
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });
        return this.http.get(this.url,options)
		        .map(this.extractData)
		        .catch(this.handleErrorObservable);
    }
	
	addBeer(Beer:Beer): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
	let token = "bearer " + this.loginService.getToken();
	headers.append('Authorization', token);
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.url, Beer, options)
               .map(this.extractData)
               .catch(this.handleErrorObservable);
	} 
	
	updateBeer(Beer:Beer): Observable<any> {
	
	let url = this.url + "/" + Beer._id;
    let headers = new Headers({ 'Content-Type': 'application/json' });
	let token = "bearer " + this.loginService.getToken();
	headers.append('Authorization', token);
    let options = new RequestOptions({ headers: headers });
    return this.http.put(url, Beer, options)
               .map(this.extractData)
               .catch(this.handleErrorObservable);
	} 
	
	deleteBeer(Beer:Beer): Observable<any> {
	
	let url = this.url + "/" + Beer._id;
    let headers = new Headers({ 'Content-Type': 'application/json' });
	let token = "bearer " + this.loginService.getToken();
	headers.append('Authorization', token);
    let options = new RequestOptions({ headers: headers });
    return this.http.delete(url,options)
               .map(this.extractData)
               .catch(this.handleErrorObservable);
	}
	
	private extractData(res: Response) {
	    let body = res.json();
        return body;
    }
    private handleErrorObservable (error: Response | any) {
		console.error(error.message || error);
		return Observable.throw(error.message || error);
    }
 
}
