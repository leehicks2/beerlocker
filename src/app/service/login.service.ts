import { Injectable } from '@angular/core';
import { Http, Response,RequestOptions,Headers } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';


import { UserDetails } from '../user';
import { LoginResponse } from '../loginresponse';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
	loginUrl = "http://test.nologo.studio:8080/api/login";

  constructor(private http:Http) { }
  
  login(UserDetails:UserDetails): Observable<LoginResponse> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.loginUrl, UserDetails, options)
               .map(this.extractData)
               .catch(this.handleErrorObservable);
	} 
	
	//Set Local Storage Token
	saveTokenToLocalStorage(token){
		localStorage.setItem("token",token);
	}
	
	//Che3ck if token exists
	checkIfTokenExists(){
		if (localStorage.getItem("token") === null) {
			return false;
		}else{
			return true;
		}
	}
	
	//Get token
	getToken(){
		return localStorage.getItem("token");	
	}
	
	private extractData(res: Response) {
	    let body = res.json();
        return body;
    }
    private handleErrorObservable (error: Response | any) {
		console.error(error.message || error);
		return Observable.throw(error.message || error);
    }
}
