import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/toPromise';

import { Post } from '../post';
import { BeerNew } from '../beernew';
@Injectable({
  providedIn: 'root'
})
export class PostService {
	//url = "https://jsonplaceholder.typicode.com/posts";
	url = "http://test.nologo.studio:8080/api/beer";
	urlOne = "https://jsonplaceholder.typicode.com/posts/1";
	constructor(private http:Http) { }
    getPosts(): Observable<BeerNew[]> {
		//let url = "https://jsonplaceholder.typicode.com/posts";
        return this.http.get(this.url)
		        .map(this.extractData)
		        .catch(this.handleErrorObservable);
    }
	getPost(): Observable<Post> {
		//let url = "https://jsonplaceholder.typicode.com/posts/1";
        return this.http.get(this.urlOne)
		        .map(this.extractData)
		        .catch(this.handleErrorObservable);
    }
	private extractData(res: Response) {
	    let body = res.json();
        return body;
    }
    private handleErrorObservable (error: Response | any) {
		console.error(error.message || error);
		return Observable.throw(error.message || error);
    }
}
