import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';


@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}
  checkAuthed = localStorage.getItem("token");
  isAuthed = false;
	
  canActivate() {
	if(this.checkAuthed != null){
		this.isAuthed = true;
	}
    if (this.isAuthed) {
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }
}
