import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { UserDetails } from '../user';
import { LoginResponse } from '../loginresponse';
import { LoginService } from '../service/login.service';
import { Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	observableLogin: Observable<LoginResponse>;
	userDetails = new UserDetails();
	loginResponse = new LoginResponse();
	errorMessage: String;
	constructor(private loginService: LoginService,private router: Router) { }

  //Login Stuff
	loginUser(){
		this.observableLogin = this.loginService.login(this.userDetails);
		this.observableLogin.subscribe(
			loginResponse => this.loginResponse = loginResponse,
            error =>  this.errorMessage = <any>error,
			() => this.saveToken(this.loginResponse.token));
	}
	
	saveToken(token){
		this.loginService.saveTokenToLocalStorage(token);
		this.router.navigate(['/beerlist']);
	}
  
  
  ngOnInit() {
	  
	  
  }

}
