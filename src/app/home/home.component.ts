import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BeerService } from '../service/beer.service';
import { Beer } from '../beer';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['../app.component.css','./home.component.css']
})
export class HomeComponent implements OnInit {
  observableBeers: Observable<Beer[]>;
  beerList:Beer[];
  
  errorMessage: String;
  
  constructor(private beerService: BeerService) { }
  
  
  //Get Beers
	//Get Beers List
	getBeersList(){
		this.observableBeers = this.beerService.getBeers();
		this.observableBeers.subscribe(
			beerList => this.beerList = beerList,
            error =>  this.errorMessage = <any>error);	
	}
  
  
  //Beer pouring animation
  togglePouring = false;
  playStarted = false;
  
  //Store Base Elements
  beerElement;
  beerPouringElement;
  beerHandleElement;
  
  startBeerPour(){
		this.beerElement.classList.add('beer');
		this.beerPouringElement.classList.add('beer-from-tap');
		this.beerHandleElement.classList.add('transformHandle');
  }
  resetBeerPour(){
		this.playStarted = false;
		this.beerElement.classList.remove('beer');
		this.beerElement.style.webkitAnimationPlayState = "running";
		this.beerPouringElement.classList.remove('beer-from-tap');
		this.beerHandleElement.classList.remove('transformHandle');
  }
  toggleBeerPour(){
	  if(!this.playStarted){
			this.startBeerPour();
			this.togglePouring = true;
			this.playStarted = true;
			return;
		}else{
		  if(this.togglePouring){
			this.beerElement.style.webkitAnimationPlayState = "paused";
			this.beerHandleElement.classList.remove('transformHandle');
			this.togglePouring = false;
		  }else{
			 this.beerElement.style.webkitAnimationPlayState = "running";
			 this.beerHandleElement.classList.add('transformHandle');
			 this.togglePouring = true;
		  }
		}
  }

  ngOnInit() {
	 this.beerElement = document.getElementById('beer');
	 this.beerPouringElement = document.getElementById('beer-pour-from-tap');
	 this.beerHandleElement = document.getElementById('beer-pourer-handle');
	 this.getBeersList();
  }

}
