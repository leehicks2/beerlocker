export class BeerNew {
	_id: number;
	name:string;
    imagePath: string;
    rating: number;
    comment: string;
	quantity:number;
	type:string;
	__v:number;
	
   constructor() { 
		this._id=0;
		this.name = "";
		this.imagePath = "";
		this.rating = 0;
		this.comment = "";
		this.quantity = 0;
		this.type = "";
		this.__v = 0;
   }
}
