import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { Beer } from '../beer';
import { BeerService } from '../service/beer.service';


@Component({
	styleUrls: ['../app.component.css','beerlist.component.css'],
	templateUrl: 'beerlist.component.html',
})

// Component class
export class BeerListComponent implements OnInit {
	
	 observableBeers: Observable<Beer[]>;
	 beerList:Beer[];
	 
	 observableAddBeer: Observable<any>;
	 addResponse="";
	 
	 newBeer = new Beer();
	 editBeer = new Beer();
	
	 listBeerView = true;
	 addBeerView = false;
	 editBeerView = false;
	 
     errorMessage: String;
	 
	constructor(private beerService: BeerService) {
	}
	
	
	//Get Beers
	//Get Beers List
	getBeersList(){
		this.observableBeers = this.beerService.getBeers();
		this.observableBeers.subscribe(
			beerList => this.beerList = beerList,
            error =>  this.errorMessage = <any>error);	
	}
	
	// NEW BEER
	// Add New Beer
	submitNewBeer(){
		this.newBeer.imagePath = "/img/1.png";
		this.observableAddBeer = this.beerService.addBeer(this.newBeer);
		this.observableAddBeer.subscribe(
			addResponse => this.addResponse = addResponse,
            error =>  this.errorMessage = <any>error,
			() =>this.setTabView('listBeerView')
			);
	}
	
	//clear the new beer object
	clearNewBeer(){
		this.newBeer = new Beer();
	}
	
	// UPDATE BEER
	// update Beer
	submitUpdateBeer(){
		
		//alert(JSON.stringify(this.editBeer));
		this.observableAddBeer = this.beerService.updateBeer(this.editBeer);
		this.observableAddBeer.subscribe(
			addResponse => this.addResponse = addResponse,
            error =>  this.errorMessage = <any>error,
			() => this.setTabView('listBeerView')
			);
	}
	
	//Set the update beer object
	setUpdateBeer(updateBeer){
		this.editBeer = updateBeer;
		this.setTabView('editBeerView');
	}
	
	//DELETE BEER
	//Delete beer
	submitDeleteBeer(beerPassed){
		this.observableAddBeer = this.beerService.deleteBeer(beerPassed);
		this.observableAddBeer.subscribe(
			addResponse => this.addResponse = addResponse,
            error =>  this.errorMessage = <any>error,
			() =>this.setTabView('listBeerView'));
	}
	
	//clear the update beer object
	clearUpdateBeer(){
		this.editBeer = new Beer();
	}
	
	
	//Set the css class for tab headers
	setTabView(tabName){
		if(tabName == 'listBeerView'){			
			this.addBeerView = false;
			this.editBeerView = false;
			this.listBeerView = true;
		}else if(tabName == 'addBeerView'){
			this.listBeerView = false;
			this.editBeerView = false;
			this.addBeerView = true;
		}else if(tabName == 'editBeerView'){
			this.listBeerView = false;
			this.addBeerView = false;
			this.editBeerView = true;
		}
	}
	
	
	ngOnInit() {
		this.getBeersList();
  }
}